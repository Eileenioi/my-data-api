<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/unemployment','UnemploymentController@all');
Route::get('/unemployment/{id}','UnemploymentController@getById');
Route::get('/unemployment/year/{year}','UnemploymentController@getDataByYear');
Route::get('/unemployment/state/{state}','UnemploymentController@getDataByState');

Route::get('/disable-person','DisabledPersonController@all');
Route::get('/disable-person/{id}','DisabledPersonController@getById');
Route::get('/disable-person/state/{state}','DisabledPersonController@getDataByState');
Route::get('/disable-person/race/{race}','DisabledPersonController@getDataByRace');

Route::get('/crude','CrudeController@all');
Route::get('/crude/year/{year}','CrudeController@getDataByYear');
Route::get('/crude/{id}','CrudeController@getById');

Route::get('/kes-mahkamah','KesMahkamahController@all');
Route::get('/kes-mahkamah/{id}','KesMahkamahController@getById');
Route::get('/kes-mahkamah/state/{state}','KesMahkamahController@getDataByState');
Route::get('/kes-mahkamah/year/{year}','KesMahkamahController@getDataByYear');
Route::get('/kes-mahkamah/category/{category}','KesMahkamahController@getDataByCategory');