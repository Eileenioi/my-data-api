<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KesMahkamah extends Model
{
    protected $table = 'kes_mahkamah';
    protected $primaryKey = 'id';
}
