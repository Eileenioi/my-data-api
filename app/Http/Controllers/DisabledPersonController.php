<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DisabledPerson;

class DisabledPersonController extends Controller
{
    public function all()
    {
        $data =  DisabledPerson::all()->groupBy('state');
        return response()->json(['year' => '2018', 'data' => $data], 200);
    }

    public function getById($id)
    {
        $data = DisabledPerson::findOrFail($id);
        return response()->json($data, 200);
    }

    public function getDataByState($state)
    {
        $data = DisabledPerson::where("state", $state)->get();
        return response()->json($data, 200);
    }

    public function getDataByRace($race)
    {
        $data = DisabledPerson::where("race", $race)->get();
        return response()->json($data, 200);
    }
}
